package AlertGUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JMenuItem;
import javax.swing.JMenu;

import java.awt.FlowLayout;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import javax.swing.UIManager;


public class CalcGUI extends JFrame implements ActionListener{

	private static int DesfRef = 0;
	private JPanel contentPane;
	private JTextField Pantalla;
	private String old;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalcGUI frame = new CalcGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws Exception 
	 */
	public CalcGUI() throws Exception {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		
		Pantalla = new JTextField();
		Pantalla.setBackground(new Color(176, 224, 230));
		Pantalla.setHorizontalAlignment(SwingConstants.RIGHT);
		Pantalla.setEditable(false);
		Pantalla.setText("0");
		contentPane.add(Pantalla);
		Pantalla.setPreferredSize(new Dimension(240, 80));
		JButton [] ArrButt = new JButton[17];
		String ArrayTxt[] = {"CE","7","8","9","/","4","5","6","x","1","2","3","-",".","0","=","+"};
		String mainBarNames[] = {"Arxiu","Edit","Mode","Ajuda"};
		String internBarNames[][] = {{"Guardar", "Obrir"},{"Desfer","Refer", "Copiar", "Enganxar",},{"Bàsic","Científic"},{"Ajuda en linea","Quant a"}};
		JMenu mainBar[] = new JMenu[mainBarNames.length];
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 12, 129, 21);
		setJMenuBar(menuBar);
		String old;
		int DesfRef = 0; 
		int x = 0;
		int k=0;
		for (int i = 0; i < internBarNames.length; i++) {
			for (int j = 0; j < internBarNames[i].length; j++) {
				x++;
			}
		}
		JMenuItem menuItem[] = new JMenuItem[x];
		for (int i = 0; i < ArrayTxt.length; i++) {
			ArrButt[i] = (JButton) new JButton(ArrayTxt[i]);
			try{
				Integer.parseInt(ArrayTxt[i]);
				ArrButt[i].setBackground(new Color(0, 255, 255));
			}catch(Exception e){
				ArrButt[i].setBackground(new Color(0, 150, 200));
			}
			ArrButt[i].setPreferredSize(new Dimension(80, 80));
			ArrButt[i].addActionListener(this);
			getContentPane().add(ArrButt[i]);
		}
		for (int i = 0; i < mainBar.length; i++) {
			mainBar[i] = (JMenu) new JMenu(mainBarNames[i]);
			menuBar.add(mainBar[i]);
			for (int j = 0; j < internBarNames[i].length; j++) {
				menuItem[k] = new JMenuItem(internBarNames[i][j]);
				mainBar[i].add(menuItem[k]);
				menuItem[k].addActionListener(this);
				k++;
			}
		}
	}
	public String calcul(String dades) throws ScriptException{
		ScriptEngineManager mgr = new ScriptEngineManager();
	    ScriptEngine engine = mgr.getEngineByName("JavaScript");
	    String foo = Pantalla.getText();
	    String[] split = foo.split("(?!^)");
	    for (int i = 0; i < split.length; i++) {
			if (split[i].equals("x")) {
				split[i] = "*";
			}
		}
	    foo = "";
	    for (int i = 0; i < split.length; i++) {
			foo = foo + split[i];
		}
	    try {
	    	return engine.eval(foo).toString();
		} catch (Exception e) {
			return "Error";
		}
	}
	public void desfer(){
		if(DesfRef == 0){
			String AUX = old;
			old = Pantalla.getText();
			Pantalla.setText(AUX);
			DesfRef = 1;
		}
	}
	public void refer(){
		if(DesfRef == 1){
			String AUX = old;
			old = Pantalla.getText();
			Pantalla.setText(AUX);
			DesfRef = 0;
		}
	}
	public void copiar(){
		StringSelection stringSelection = new StringSelection (Pantalla.getText());
		Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
		clpbrd.setContents (stringSelection, null);
	}
	public void enganxar(){
		Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
	    Transferable t = c.getContents(this);
	    if (t == null)
	        return;
	    try {
	        Pantalla.setText((String) t.getTransferData(DataFlavor.stringFlavor));
	    } catch (Exception e){
	        e.printStackTrace();
	    }//try
	}
	public void llicencia(){
		DialGUI x = new DialGUI();
		x.setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() instanceof JButton){
			JButton Butt = (JButton)e.getSource();
			String a = Butt.getText();
			old = Pantalla.getText();
//			if (a.equals("x")) {
//				a = "*";
//			}
			if (a.equals("CE")) {
				Pantalla.setText("0");
			}
			else if(a.equals("=")){
				String calc = "";
				try {
					calc = calcul(Pantalla.getText());
				} catch (ScriptException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            	Pantalla.setText(calc);
			}
			else{
				Pantalla.setText(Pantalla.getText() + a);
			}
		}
		else if (e.getSource() instanceof JMenuItem) {
			JMenuItem Item = (JMenuItem)e.getSource();
			String text = Item.getText();
			if (text.equals("Quant a")) {
				llicencia();
			}
			else if (text.equals("Desfer")) {
				desfer();
			}
			else if (text.equals("Refer")) {
				refer();
			}
			else if (text.equals("Copiar")) {
				copiar();
			}
			else if (text.equals("Enganxar")) {
				enganxar();
			}
		}
	}
}
